# auto_volcano_build

A summary of scripts to build volcano plots based on catalytic cycles of homogeneous catalysts for my work at the LCMD group at EPFL.

The scripts directory contains all the necessary scripts, and might be saved to any location on the system.

All scripts are based on your working directory. Please be sure that your working diretory contains a directory named 'data' with the input. The other specifications are mentioned in the script itself.
This script was written in order to automize the procedure of generating diffrent types of volcano plots as used by the LCMD group at EPFL.