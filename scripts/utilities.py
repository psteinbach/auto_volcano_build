# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 17:51:01 2020

@author: Pit Steinbach
"""
import numpy as np
import scipy.constants as sc 

def read_in_file(csv_file):
    with open('./data/'+csv_file,'r') as init_data:
        energy = np.genfromtxt(init_data,delimiter=',',skip_header=1)
        energy= energy[0:,1:]
        init_data.close()
    with open('./data/'+csv_file,'r') as init_data:
        name = np.genfromtxt(init_data,delimiter=',',max_rows=1,dtype=str)
        name= name[1:]
        init_data.close()
    return name, energy

def calc_pds_points(array):
    Q_reaction=np.zeros((len(array)))
    for i in range(1,len(array)):
        Q_reaction[i-1]=(array[i-1]-array[i])
    numb=np.argsort(Q_reaction)[:1]
    #print(Q_reaction)
    Q_pds=Q_reaction[numb]
    return Q_pds  

def calc_TOF(array,Delta_G_reaction,T,coeff,exact):
    h= sc.value('Planck constant')
    k_b=sc.value('Boltzmann constant')
    R=sc.value('molar gas constant')   
    n_I=coeff.count(0)
    X_TOF=np.zeros((n_I-1,2))
    if exact:
        
        matrix_T_I=np.zeros((n_I-1,2))
        j=0
        for i in range(0,len(array)-1):
            if coeff[i]==1:
                matrix_T_I[(j,1)]=array[i]
                j+=1
            else:
                if coeff[i+1]==0:
                    matrix_T_I[(j,0)]=array[i]
                    matrix_T_I[(j,1)]=array[i]
                    j+=1
                else:
                    matrix_T_I[(j,0)]=array[i]
        sum_span=0
        for i in range(0,n_I-1):
            for j in range(0,n_I-1):
                if i > j:
                    sum_span+=np.exp(((matrix_T_I[(i,1)]-matrix_T_I[(j,0)]-Delta_G_reaction)*4184)/(R*T))
                else:
                    sum_span+=np.exp(((matrix_T_I[(i,1)]-matrix_T_I[(j,0)])*4184)/(R*T))
        TOF=((k_b*T)/h)*((np.exp((-Delta_G_reaction*4184)/(R*T)))/sum_span)
        for i in range(0,n_I-1):
            sum_e=0
            for j in range(0,n_I-1):
                if i > j:
                    sum_e+=np.exp(((matrix_T_I[(i,1)]-matrix_T_I[(j,0)]-Delta_G_reaction)*4184)/(R*T))
                else:
                    sum_e+=np.exp(((matrix_T_I[(i,1)]-matrix_T_I[(j,0)])*4184)/(R*T))
            X_TOF[(i,1)]=round(sum_e/sum_span,2)
        for j in range(0,n_I-1):
            sum_e=0
            for i in range(0,n_I-1):
                if i > j:
                    sum_e+=np.exp(((matrix_T_I[(i,1)]-matrix_T_I[(j,0)]-Delta_G_reaction)*4184)/(R*T))
                else:
                    sum_e+=np.exp(((matrix_T_I[(i,1)]-matrix_T_I[(j,0)])*4184)/(R*T))
            X_TOF[(j,0)]=round(sum_e/sum_span,2)
    else:
        delta_E_matrix= np.zeros((len(array),len(array)))
        for i in range(0,len(array)-1):
            for j in range(0,len(array)-1):
                if not(coeff[i]==coeff[j]):
                    if i < j:
                        delta_E_matrix[i,j]=array[i]-array[j]+Delta_G_reaction
                    else:
                        delta_E_matrix[i,j]=array[i]-array[j]
        Energy_Span=np.amax(delta_E_matrix)
        print(Energy_Span)
        TOF=((k_b*T)/h)*np.exp((-(Energy_Span*4184)/(R*T))) #s^-1
    return TOF,X_TOF