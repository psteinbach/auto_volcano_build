# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 17:19:46 2020

@author: Pit Steinbach
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from matplotlib import cm
import utilities as u
import pathlib
import matplotlib
plt.rcParams.update({'font.size': 10})
plt.rcParams.update({'savefig.dpi': 300})

file_name='Ligand_overview' #fill in the name of your data which should be a csv and has to be placed in a directory called 'data', 
#where the columns are filled with the energy for a given TS or intermediate(the first column should contain the names 
#of different entries in the rows). The rows contain diffrent complexes either with cganging ligand or substrate. 
#For an example see data_example.csv
# The script expects energy values in kcal/mol.


construct_volcano=True #determine weither to construct the actual volcano plots. You shoould first start without constrcuting them and choose your
#potential descriptors based on the R^2 values, whoich are calculated and written to the 'LFESR.csv' file.
which_descriptor_plots=['5'] #fill in which descriptors shoudl be used to constrcut LFESR plots or volcano plots.

print_LFESR_plots=True #determine weither to print the LFESR plots for the chosen descriptors or not
show_volcano_allreactions=True #determine to print the Volcano plots where alle possible reactions are displayed
exact_TOF=[True] #you can choose to use the exact TOF equation or not, two options are possible



Delta_G_formation=-28.1 #in kcal per mol
xmax=60             #define your range of descriptor values with same unit then descriptor 
xmin=-60           #define your range of descriptor values with same unit then descriptor 
npoints=500         #define the number of points that are used to construct the energy volcanoes
xmax_TOF=20       #define your range of descriptor values with same unit then descriptor 
xmin_TOF=-40        #define your range of descriptor values with same unit then descriptor  
n_points_TOF=2000   #define the number of points that are used to construct the energy volcanoes
Temp=380 #in K; reaction temperature; needed for TOF calculations 

color_lines= cm.viridis(0.2)
color_data_points='tab:orange'

###########################################################################################################
#Script starts here
###########################################################################################################
#First the linear reaction relationships are constrcuted, a csv with the R^2 values is generated
###########################################################################################################
if not(file_name.endswith('.csv')):
    file_name=file_name+'.csv'
if construct_volcano or print_LFESR_plots:   
    pathlib.Path('./graphs').mkdir(parents=True, exist_ok=True)
    for elements in which_descriptor_plots:
        pathlib.Path('./graphs/descriptor_'+elements).mkdir(parents=True, exist_ok=True)
        pathlib.Path('./graphs/descriptor_'+elements).mkdir(parents=True, exist_ok=True)
        pathlib.Path('./graphs/descriptor_'+elements).mkdir(parents=True, exist_ok=True)
        
x=np.linspace(xmin,xmax,npoints)

names, energies = u.read_in_file(file_name)

print(names,'\n',energies)

slopes=[0]
intercepts=[0]

with open ('LFESR.csv','w') as output_table:
    output_table.write('')
    for i in range(0,len(names)):
        output_table.write(',R^2 ('+names[i]+')')
    output_table.write(',<R^2>\n')
    for j in range(0,len(names)):
        r_aver=0
        output_table.write('descriptor: '+names[j])
        for i in range(0,len(energies[1,0:])):
            slope, intercept, r_value, p_value, std_err = stats.linregress(energies[0:,j], energies[0:,i])
            dy = energies[0:,j]*slope+intercept
            
            ddy = np.mean((energies[0:,i]-dy)**2)
            std =np.sqrt(ddy)
            r_aver+=r_value**2
            
            output_table.write(','+str(round(r_value**2,2)))
            if print_LFESR_plots and (names[j] in which_descriptor_plots):
                slopes.append(slope)
                intercepts.append(intercept)
                x_maxi=max(energies[0:,j])
                x_mini=min(energies[0:,j])
                pathlib.Path('./graphs/descriptor_'+names[j]+'/LFESR'+names[j]).mkdir(parents=True, exist_ok=True)
                plt.plot(energies[0:,j], energies[0:,i],'x',c=color_data_points)
                plt.plot(energies[0:,j],slope*energies[0:,j]+intercept,c=color_lines)
                yfit=[slope*x_mini+intercept,slope*x_maxi+intercept]
                plt.fill_between([x_mini,x_maxi], yfit - std, yfit + std, color='gray', alpha=0.15)
                plt.xlim((x_mini,x_maxi))
                x0, xmax = plt.xlim()
                y0, ymax = plt.ylim()
                data_width = xmax - x0
                data_height = ymax - y0
                plt.text(x0 + data_width *0.01, y0 + data_height * 0.94, '$\Delta\,G_{RRS}('+names[i]+')$ ='+str(round(slope,3))+'$\Delta\,G_{RRS}('+names[j]+') + $'+str(round(intercept,2))+' kcal mol$^{-1}$')
                plt.text(x0 + data_width *0.01, y0 + data_height * 0.88, '$R^2$= '+str(round(r_value**2,2)))
                plt.text(x0 + data_width *0.01, y0 + data_height * 0.82, '$2\sigma$= '+ str(round(2*np.sqrt(ddy),2))+' kcal mol$^{-1}$')
                plt.xlabel('$\Delta\,G_{RRS}('+names[j]+')$ / kcal mol$^{-1}$')
                plt.ylabel('$\Delta\,G_{RRS}$('+names[i]+') / kcal mol$^{-1}$')
                plt.savefig('graphs/descriptor_'+names[j]+'/LFESR'+names[j]+'/LFESR_'+names[i]+'.png',dpi=300,bbox_inches='tight')
                plt.draw()
                plt.pause(0.001)
                plt.close()
        output_table.write(', '+str(round((r_aver/(len(energies[1,0:]))),2))+'\n')
        
#################################################################################################################################################
# Thermodynamic Volcanoes are constrcuted
#################################################################################################################################################
if construct_volcano:
    temp=['1']
    for elements in names:
        if not (elements.startswith('TS')):
            temp.append(elements)
    
    for elements in names:
        if (elements in which_descriptor_plots):
            ts_coefficient=[0]
            g=1
            slopes=[0]
            intercepts=[0]
            for i in range(0,len(energies[1,0:])):
                if names[i]==temp[g]:
                    slope, intercept, r_value, p_value, std_err = stats.linregress(energies[0:,int(np.where(names==elements)[0])], energies[0:,i])
                    slopes.append(slope)
                    intercepts.append(intercept)
                    ts_coefficient.append(0)
                    g+=1
                else:
                    ts_coefficient.append(1)
                if g>4:
                    g=4
            ts_coefficient.append(0)
            pathlib.Path('./graphs/descriptor_'+elements+'/Thermodynamic_Volcano').mkdir(parents=True, exist_ok=True)
            if show_volcano_allreactions:
                slopes_thermo_volcano=[]
                intercepts_thermo_volcano=[]
                names_thermo=[]
                for i in range(1,len(slopes)):    
                    slopes_thermo_volcano.append(slopes[i-1]-slopes[i])
                    intercepts_thermo_volcano.append(intercepts[i-1]-intercepts[i])
                    names_thermo.append(temp[i-1]+'-'+temp[i])
                slopes_thermo_volcano.append(slopes[-1]-slopes[0])
                intercepts_thermo_volcano.append(intercepts[-1]-intercepts[0]-Delta_G_formation)
                names_thermo.append(temp[-1]+'-'+temp[0])
                plt.title('Thermodynamic volcano')    
                for i in range(0,len(temp)):
                    color= cm.viridis(i*0.27)
                    plt.plot(x,slopes_thermo_volcano[i]*x+intercepts_thermo_volcano[i],c=color,label=names_thermo[i])
                plt.legend(loc=2,bbox_to_anchor=(1.05, 1))
                plt.xlabel('$\Delta\,G_{RRS} ('+elements+')$ / kcal mol$^{-1}$')
                plt.ylabel('$- \Delta\,G_{pds}$ / kcal mol$^{-1}$')
                plt.savefig('graphs/descriptor_'+elements+'/Thermodynamic_Volcano/Thermodynamic_volcano_all_reactions.png',bbox_inches='tight')
                plt.draw() 
                plt.pause(0.001)
                plt.close()
            input_pds=np.zeros(len(temp)+1)
            input_pds[-1]=Delta_G_formation
            y=[]
            for i in range(0,len(x)):
                    for j in range(1,len(input_pds)-1):
                        input_pds[j]=slopes[j]*x[i]+intercepts[j]
                    y.append((u.calc_pds_points(input_pds)))
                
            plt.plot(x,y,c=color_lines)
            plt.title('Thermodynamic volcano') 
            plt.xlabel('$\Delta\, G_{RRS} ('+elements+')$ / kcal mol$^{-1}$')
            plt.ylabel('$- \Delta\, G_{pds}$ / kcal mol$^{-1}$')
            plt.savefig('graphs/descriptor_'+elements+'/Thermodynamic_Volcano/Thermodynamic_volcano_final.png',bbox_inches='tight')
            plt.draw() 
            plt.pause(0.001)
            plt.close()
            if not(names[0]=='1'):
                fullnames=[]
                fullnames.append('1')
                for i in range(0,len(names)):
                    fullnames.append(names[i])
                    
    ###########################################################################################################
    #Combined volcanoes are constrcucted in this part
    ###########################################################################################################
                    
            slopes=[0]
            intercepts=[0]
            
            for i in range(0,len(energies[1,0:])):
                slope, intercept, r_value, p_value, std_err = stats.linregress(energies[0:,int(np.where(names==elements)[0])], energies[0:,i])
                slopes.append(slope)
                intercepts.append(intercept)
                
            slopes_combined_volcano=[]
            intercepts_combined_volcano=[]
            names_combined=[]
            
            for i in range(1,len(energies[1,0:])+1):
                slopes_combined_volcano.append(slopes[i-1]-slopes[i])
                intercepts_combined_volcano.append(intercepts[i-1]-intercepts[i])
                names_combined.append(fullnames[i-1]+'-'+fullnames[i])
            slopes_combined_volcano.append(slopes[-1]-slopes[0])
            intercepts_combined_volcano.append(intercepts[-1]-intercepts[0]-Delta_G_formation)
            names_combined.append(fullnames[-1]+'-'+fullnames[0])
            #print(names_combined)   
            pathlib.Path('./graphs/descriptor_'+elements+'/Combined_Volcano').mkdir(parents=True, exist_ok=True)
            #plots all the lines of the combined volcano
            if show_volcano_allreactions:
                for i in range(0,len(energies[1,0:])+1):
                    color= cm.viridis(i*0.15)
                    plt.plot(x,slopes_combined_volcano[i]*x+intercepts_combined_volcano[i],c=color,label=names_combined[i])
                plt.legend(loc=2,bbox_to_anchor=(1.05, 1))
                plt.title('Combined Volcano')
                plt.xlabel('$\Delta\, G_{RRS} ('+elements+')$ / kcal mol$^{-1}$')
                plt.ylabel('$- \Delta\, G_{pds}$ / kcal mol$^{-1}$')
                plt.savefig('graphs/descriptor_'+elements+'/Combined_Volcano/combined_volcano_all_reactions.png',bbox_inches='tight')
                plt.draw() 
                plt.pause(0.001)
                plt.close()
                
            input_pds=np.zeros(len(energies[1,0:])+2)
            input_pds[-1]=Delta_G_formation
            y=[]
            
            for i in range(0,len(x)):
                    for j in range(1,len(input_pds)-1):
                        input_pds[j]=slopes[j]*x[i]+intercepts[j]
                    y.append((u.calc_pds_points(input_pds)))
            
            Q_pds=[]
            #print(energies[1,0:])
            
            
            for i in range(0,len(energies[0:,int(np.where(names==elements)[0])])):
                for j in range(0,len(energies[1,0:])):     
                    input_pds[j+1]=energies[i,j]
                Q_pds.append((u.calc_pds_points(input_pds)))
            #print(Q_pds)    
            
            plt.title('Combined Volcano')
            plt.scatter(energies[0:,int(np.where(names==elements)[0])],Q_pds, c=color_data_points,marker='+')
            plt.plot(x, y,c=color_lines)
            plt.xlabel('$\Delta\, G_{RRS} ('+elements+')$ / kcal mol$^{-1}$')
            plt.ylabel('$- \Delta\, G_{pds}$ / kcal mol$^{-1}$')
            plt.savefig('graphs/descriptor_'+elements+'/Combined_Volcano/combined_volcano_final.png')
            plt.draw() 
            plt.pause(0.001)
            plt.close()
            
    #########################################################################################
          #Constructing the TOF Volcanoes 
    #########################################################################################
            pathlib.Path('./graphs/descriptor_'+elements+'/TOF_Volcano').mkdir(parents=True, exist_ok=True)
            for element in exact_TOF:
    
                 # number of points that should be drawn for the volcano in interval [xmin_TOF,xmax_TOF]
                x_TOF=np.linspace(xmin_TOF,xmax_TOF,n_points_TOF)
    
                input_TOF=np.zeros(len(energies[1,0:])+2)
                input_TOF[-1]=Delta_G_formation
                TOFs=np.zeros(len(x_TOF))
                TOFS_points=[]
                
                # now the points for the volcano are computed using the scaling relationships and the TOF function
                for i in range(0,len(x_TOF)):
                    for j in range(0,len(slopes)):
                        input_TOF[j]=slopes[j]*x_TOF[i]+intercepts[j]
                    #print(input_TOF)
                    TOFs[i],trash=u.calc_TOF(input_TOF,Delta_G_formation,Temp,ts_coefficient,element)
                
                # this calculates the points 
                for i in range(0,len(energies[0:,int(np.where(names==elements)[0])])):
                    for j in range(0,len(energies[1,0:])):     
                        input_TOF[j+1]=energies[i,j]
                    point,X_TOFS=u.calc_TOF(input_TOF,Delta_G_formation,Temp,ts_coefficient,element)
                    print(point)
                    TOFS_points.append(point)
                    #if element:
                        #print(X_TOFS)
                
                if element:
                    title='TOF volcano with exact equation'
                    path='exact'
                else:
                    title = 'TOF volcano with approximation'
                    path = 'approximated'
                
                plt.plot(x_TOF,TOFs,c=color_lines)
                plt.title(title)
                plt.plot(energies[0:,int(np.where(names==elements)[0])],TOFS_points,'+',c=color_data_points)
                plt.xlim(xmin_TOF,xmax_TOF)
                plt.yscale('log')
                plt.xlabel('$\Delta\, G_{RRS} ('+elements+')$ / kcal mol$^{-1}$')
                plt.ylabel('$TOF$ / s$^{-1}$')
                plt.savefig('graphs/descriptor_'+elements+'/TOF_Volcano/TOF_volcano_'+path+'.png',bbox_inches='tight')
                plt.draw() 
                plt.pause(0.001)
                plt.close()
